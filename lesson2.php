<?php

namespace Lesson2;

use Exception;
use Lesson1\Person;

require 'lesson1.php';

class User extends Person
{

    public function __construct($name, $amount = 0)
    {
        $this->setName($name);
        $this->setAmount($amount);
    }

    public function printStatus()
    {
        echo $this->__toString();
    }

    public function __toString()
    {
        $amount = round($this->getAmount(), 2);
        return "У пользователя {$this->getName()} сейчас на счету \$$amount";
    }


    /**
     * Check amount parameter before giving it to someone
     * @param $amount
     * @throws Exception
     */
    private function checkAmountToGive($amount)
    {
        if (!is_numeric($amount)) {
            throw new Exception('Amount field should be numeric only');
        }
        if ($amount > $this->getAmount()) {
            throw new Exception('You cannot give more money than you have');
        }
        if ($amount < 0) {
            throw new Exception('Amount should be bigger than 0');
        }
    }

    /**
     * Gives money to another user
     * @param User $recipient
     * @param int $amount
     */
    public function giveMoney(User $recipient, $amount = 0)
    {
        try {
            $this->checkAmountToGive($amount);
            $recipient->setAmount($recipient->getAmount() + $amount);
            $this->setAmount($this->getAmount() - $amount);
            echo "Пользователь {$this->getName()} перечислил \${$amount} пользователю {$recipient->getName()} \n";
        } catch (Exception $e) {
            echo $e->getMessage() . "\n";
            return;
        }
    }


}

abstract class Product
{
    private $price, $name, $owner;

    public static $products = [];

    public function getPrice()
    {
        return $this->price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function __construct(int $price, string $name, User $owner)
    {
        $this->price = $price;
        $this->name = $name;
        $this->owner = $owner;
    }

    public static function registerProduct($product)
    {
        $check = false;
        foreach (self::$products as $item) {
            if ($item === $product) {
                $check = true;
            }
        }
        if (!$check) {
            self::$products[] = $product;
        }
    }

    public function __toString()
    {
        return "Product: $this->name, price: $this->price";
    }

    public static function getProductsIterator()
    {
        return new class (self::$products) implements \Iterator
        {

            private $var = [];

            public function __construct(array $products)
            {
                $this->var = $products;
            }

            public function rewind()
            {
                reset($this->var);
            }

            public function current()
            {
                $var = current($this->var);
                return $var;
            }

            public function key()
            {
                $var = key($this->var);
                return $var;
            }

            public function next()
            {
                $var = next($this->var);
                return $var;
            }

            public function valid()
            {
                $key = key($this->var);
                $var = ($key !== NULL && $key !== FALSE);
                return $var;
            }

        };
    }
}

class Processor extends Product
{
    private $frequency;

    public function __construct($price, $name, $owner)
    {
        parent::__construct($price, $name, $owner);
    }

}

class Ram extends Product
{
    private $type;
    private $memory;

    public function __construct($price, $name, $owner, string $type, string $memory)
    {
        $this->type = $type;
        $this->memory = $memory;

        parent::__construct($price, $name, $owner);
    }
}


$u1 = new User('Vasya', 100);
$u2 = new User('Petya', 100);
$a1 = new Processor(100, 'Ryzen 5 2600', $u1);
$a2 = new Ram(90, 'Ballistics G-Skill', $u2, 'dd4', '16gb');
Product::registerProduct($a1);
Product::registerProduct($a2);
foreach (Product::getProductsIterator() as $key => $item) {
    echo $item . ' Владелец: ' . $item->getOwner()->getName() . "\n";
}
