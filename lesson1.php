<?php


abstract  class Person {
    private $name;
    private $amount;

    /**
     * Getter for the name field
     * @return mixed
     */
    public function getName(){
        return $this->name;
    }

    /**
     * Setter for the name field
     * @param $name
     * @throws Exception
     */
    protected function setName($name){
        if (empty($name)){
            throw new Exception('Name field cannot be empty');
        }
        $this->name = $name;
    }

    /**
     * Getter for the amount field
     * @return mixed
     */
    public function getAmount(){
        return $this->amount;
    }

    /**
     * Setter for the amount field
     * @param $amount
     */
    protected function setAmount($amount){
        $this->amount = $amount;
    }


    public abstract function printStatus();

}


interface MoneyGiver {
    public function giveMoney(User $recipient,$amount = 0);
}

class User extends Person implements MoneyGiver{

    public function __construct($name,$amount=0)
    {
        $this->setName($name);
        $this->setAmount($amount);
    }

    public function printStatus(){
        $amount = round($this->getAmount(),2);
        echo "У пользователя {$this->getName()} сейчас на счету \$$amount \n";
    }

    
    /**
     * Check amount parameter before giving it to someone
     * @param $amount
     * @throws Exception
     */
    private function checkAmountToGive($amount){
        if (!is_numeric($amount)){
            throw new Exception('Amount field should be numeric only');
        }
        if ($amount > $this->getAmount()){
            throw new Exception('You cannot give more money than you have');
        }
        if ($amount<0){
            throw new Exception('Amount should be bigger than 0');
        }
    }

    /**
     * Gives money to another user
     * @param User $recipient
     * @param int $amount
     */
    public function giveMoney(User $recipient,$amount=0){
        try {
            $this->checkAmountToGive($amount);
            $recipient->setAmount($recipient->getAmount()+$amount);
            $this->setAmount($this->getAmount()-$amount);
            echo "Пользователь {$this->getName()} перечислил \${$amount} пользователю {$recipient->getName()} \n";
        } catch (Exception $e) {
            echo $e->getMessage()."\n";
            return;
        }
    }
}

$a = new User('test',100);
$a->printStatus();
$b = new User('abc',999);
$b->printStatus();
echo "\n";

$a->giveMoney($b,1.5);
$a->printStatus();
$b->printStatus();
echo "\n";

$a->giveMoney($b,100);
echo "\n";

$a->giveMoney($b,98.1);
$a->printStatus();
$b->printStatus();
echo "\n";


